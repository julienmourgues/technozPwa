import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

import { AppModule } from './app.module';

enableProdMode();
platformBrowserDynamic().bootstrapModule(AppModule);

const applicationServerPublicKey = 'BGmThH3PCs1CJD6c_EjMC0hKpLG3e4AMuJrZpDl-vuiUEBUsCPNISWk3LUehEpie5OZsFBvCX4tt5h29s6ULhjo';

let isSubscribed = false;

if ('serviceWorker' in navigator) {
    console.log('Service Worker and Push is supported');
  
    navigator.serviceWorker.register('service-worker.js')
    .then(function(swReg) {
        console.log('Service Worker is registered', swReg);
        if ('PushManager' in window) {
            navigator.serviceWorker.ready.then(function(swReg) {
                swReg.pushManager.getSubscription()
                    .then(function(subscription) {
                    isSubscribed = !(subscription === null);
                
                    if (isSubscribed) {
                        console.log('User IS subscribed.');

                        //On renvoie la souscription
                        updateSubscriptionOnServer(subscription);
                    } else {
                        console.log('User is NOT subscribed.');
                        const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
                        swReg.pushManager.subscribe({
                            userVisibleOnly: true,
                            applicationServerKey: applicationServerKey
                        })
                        .then(function(subscription) {
                            console.log('User is subscribed.');

                            updateSubscriptionOnServer(subscription);

                            isSubscribed = true;
                        })
                        .catch(function(err) {
                            console.log('Failed to subscribe the user: ', err);
                        });
                    }

                });
            });
            
        } else {
            console.warn('Push messaging is not supported');
        }
    })
    .catch(function(error) {
      console.error('Service Worker Error', error);
    });

} else {
    console.warn('Service worker is not supported');
}


function updateSubscriptionOnServer(subscription) {
    var json = JSON.stringify(subscription);
    var data = new FormData();
    data.append("subscription", json );
    console.log(json);
    fetch("/subscription", {
        method: "POST",
        body: data
    }).then(function(res){ 
        console.log("Subscription envoyée. Réponse : ", res); 
    });
}

function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');
  
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
  
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }