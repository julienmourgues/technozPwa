const express = require('express')
const path = require('path')
const multer = require('multer');
const request = require('request');
const sslRedirect = require('heroku-ssl-redirect');

const PORT = process.env.PORT || 5000;

var subscriptions = [];
const PUBLIC_KEY = 'BGmThH3PCs1CJD6c_EjMC0hKpLG3e4AMuJrZpDl-vuiUEBUsCPNISWk3LUehEpie5OZsFBvCX4tt5h29s6ULhjo';
const PRIVATE_KEY = 'Y0YDMks0FBEe2vacpfqZ6i9hDrq0rI_U_oYk1675eFk';

var app = express();

// enable ssl redirect
app.use(sslRedirect());

app.use(multer().array()); 
app.use(express.static(path.join(__dirname, 'www')));


app.post("/subscription", function (req, res) {
  
  var s = req.body.subscription;
  var id = JSON.parse(s).keys.auth;

  if (subscriptions.find(s => s.id === id)) {
    res.status(200).send("Souscription déjà enregistrée.");
    return;
  }

  console.log("Enregistrement d'une nouvelle souscription ", s);

  subscriptions.push({
    id,
    created: new Date(),
    subscription: s
  });
  res.status(200).send("OK");
});

app.get("/subscription", function (req, res) {
  res.status(200).send(subscriptions);
});

app.get("/subscription/broadcast", function (req, res) {

  if (subscriptions.length === 0) {
    res.status(200).send("Pas de souscriptions. Pas notifications à envoyer.");
  }

  console.log("Envoi des notifications.");
  var msg = req.query.msg || 'Lien vers le gitlab du Technoz.';

  subscriptions.forEach(
    s => {
      console.log("Envoi notification vers ", s);
      request({
        method: 'POST',
        url: 'https://web-push-codelab.glitch.me/api/send-push-msg',
        headers: {
          'Content-Type': 'application/json'
        },
        json: true,
        body: { 
          subscription: JSON.parse(s.subscription),
          data: msg,
          applicationKeys: {
            public: PUBLIC_KEY,
            private: PRIVATE_KEY
          }
        }
      },
      function (error, response, body) {
          if (!error && response.statusCode == 200) {
              console.log("Notification envoyée !", body);
          } else {
            error && console.error(error);
          }
      }
    );
  });

  res.status(200).send("Envoi de "+subscriptions.length+ " notification(s) en cours...");
});

app.listen(PORT, () => console.log(`Listening on ${PORT}`));
