<style>
    img { 
        max-width:  600px;
        max-height:  800px;
        display: inline-block;
    }

    .reveal section img {
        border: none;
    }

    div.comparatif {
        font-size: 0.5em;
    }

    span.ok {
        display: inline-block;
        width:16px;
        height:16px;
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABWklEQVQ4jYXTsUtVYRgG8N+9FxpKIhsucnEzQSjQQXAKTms4S5gEDc5hS7S9S4Eg+gc0XbEM1y6tugiBDY0hBg0hIg0hIg0iDue78nE8dV84cL7nfZ7nO8/LexrqKjzBEtYSUr6Hj1VqIxPdwRRG8A5DFe4pFnGEb8IfaGaEV9jGZo1YwjYT53UfzA0e1MaprytuQ+gos84NEJ3hZnbewlJLYREvB4i/YwK3MJOw+zhsojdAfI7nwgnuVnq9pnLqef3GCi7SeVX4IjzGQoXbaSn00M7AF8Kywk+MY15hCJ9xu2Iw3US3Ar4RJoR1zAh/lUMerYnXLRcpvMd81jjEI2FfmMWnGvEH4Wl/Dy4qzQ62hWfKrfxn9Q128avGpOv6kCXuLrTAjq8Ka7iBh/+7EW8xK+yR/0wQ2viBY2Wse6lzkL62jXHhqBqhb3CMYWEMk9hIz2TChnMxXAJtlEmc0TeOvwAAAABJRU5ErkJggg==')
    }

    span.ko {
        display: inline-block;
        width:16px;
        height:16px;
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABE0lEQVQ4jaWTsWoCURRED0EkyBbBDwhBxEJCEAsLf1BEUlgE8QusxMIiRbASEbEQSWURUoQUKUREUgWZNLvZm+tbU2ThFo+dmX0zsxfcI6gIOoKVYB/Ps6ArqHq8JeYEbcGXQBlzFDwILkPk0Rmin6dfIoKWeXkQvAVIO8G7OfcTctlcey9oCq4FLwa8FdRjbCJ+FNz5r68EUSyciGwFdWN1bPA9BHN31akTqRnywGE3CD4Cfn9EzpAl+LzIaLUK3JhzEWhk9b9wqt5zwWVyYsGGuHPkQSCTVx9iydS4FlwFPE8FkSAveDQ11hIbbQNeCoaBwGauwv5/fuWJMvaho7+XqXdCdkIVwb3SdT4oXedbj/8G3YhVhG0/RlMAAAAASUVORK5CYII=');
    }

    span.warning {
        display: inline-block;
        width:16px;
        height:16px;
        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA+0lEQVQ4jbXSvUoDQRQF4G9DSBksJIVY+lcExICNpYVY+Abbio1/lViKhaWk8hHmaexUBLEXCWJhHRwLJ7Aks0ssPHAZ5txz7j0Dw38iBjsx2G3SFA3mFu7RwVZRGud0rYbhhxigj6M/JYjBAl7QS9QH1ovS57wJripmWMT1XAlisIFHtHGc6DuMsVmUnmsTxACGyQxPqSRumDS1TzjAfi5qwl7SzA6IQQe3DeYJhkk7k+AEa1Pi5VRVrOBscinS9h5e0Z0SP6Ql/Sn+C6tFaTRJcJMxwzlOM3w3eRQxGPj9srk/8ZbOpUzvG9ttXGCUEagMfa/pX9bw8+MHDO4xztFfF/8AAAAASUVORK5CYII=');
    }
    
    .reveal div.left {
        float: left;
        width: 50%;
    }
    
    .reveal div.right {
        float: right;
        width: 50%;
    }
    
</style>

@title[Introduction]
# Progressive Web Apps

### La mort des stores et applications natives ?
### Ionic : la solution hybride miracle ?

Note:
R : Bah pourquoi est-ce la mort des applications natives ?
---

@title[Chiffres]
## Applications natives - Quelques chiffres

* **60% des applications** publiées n'ont jamais été téléchargées
* **65% des utilisateurs** de smartphone télécharge **0** application par mois
* **6 clics** sont nécessaires pour installer une application
* **80% des utilisateurs** téléchargeant une application deviennent des utilisateurs inactifs au bout de **7 jours**

Note:
R: Et les PWA dans tout ça ?
---

@title[Définition]
## Définition
> PWA est une application web qui associe le meilleur du web avec le meilleur des applications natives

Note:
R: Ca date de quand ?
---

@title[Historique]

## Historique
- Terme inventé en 2015
- Annoncé au [Google I/O 2017](https://youtu.be/m-sCdS0sQO8)

Note:
R: C'est quoi les caractéristiques ?
---

@title[Caractéristiques]
## Caractéristiques

![Caractéristiques PWA](assets/images/pwa-hero.jpg "Caractéristiques PWA")

Note:
Progressive : fonctionne avec n'importe quel appareil<br/>
Hors ligne : fonctionne hors ligne ou avec une couverture réseau faible<br/>
"App-like" : ressemble à une application native<br/>
Fresh : Mise à jour transparente<br/>
Découvrable : trouvé à partir d'un moteur de recherche<br/>
Réengageable : possibilité de recevoir des notifications même si l'application n'est pas ouverte<br/>
Installable : installé sur l'écran d'accueil d'un smartphone<br/>
Linkable: chaque état de l'application peut être partagé ou référencé <br/>
<br/>
R : concrètement, ça rend quoi ?
---

@title[Démo PWA]
## [Démo PWA](https://technoz-pwa.herokuapp.com/)
### bit.ly/technoz-pwa
<a href='http://www.unitag.io/qrcode'><img src='http://www.unitag.io/qreator/generate?crs=xnjFkEn%252FP85fCPDXJ%252FXXKnPnKU%252FtWVh9E7ei8Ex%252BR4XsTvus59MiRl4OtJ5Y%252F3aRXopA7Qn4wJ6m3qLfsP4IWv39ocSd3mMczmj1AuyiW6K%252F58n8n8s5NK61vAUi6GUR9QhYs1xUoNWG3PC4owAgU1Q%252FHThW3FIfdeEUqZ%252BlJgc%253D&crd=fhOysE0g3Bah%252BuqXA7NPQ9ALBD3OTArLfUNMe%252FHowckOuz7deRmkvZbGSlfBPVCPZS0HCgURLC9ZDs28ZU%252F9Pw%253D%253D' alt='QR Code'/></a>

Note:
R : C'est quoi le meilleur web ?
---

@title[Possibilités du Web]
## Le Web, c'est déjà ...

- Responsive
- Discoverable
- Linkable
- Safe

Note:
R : C'est tout ?

+++

## Possibilités du Web

![What web can do today ?](assets/images/wwcdt.png "What web can do today ?")

Note:
Avertissement : fonctionanlités PWA non disponible sous Safari < 11.3<br>
Add To Home Screen et SW disponbile sous Safari 11.3 qui est introduit avec macOS 10.13.4 et iOS 11.3 en bêta<br>
R: C'est quoi les réussites avec les PWA ?
---

@title[Réussites]
## Flipkart (e-commerce / Inde)

![Flipkart](assets/images/flipkart.png "Flipkart")

* 76% Temps passé sur Flipkart lite vs. expériences mobile précédentes:
* 3x plus de temps passé sur le site;
* 40% en plus de taux d'engagement
* 70% en plus de taux de conversion entre ceux arrivants via "Add to Homescreen"

+++

## Alibaba (e-commerce / Chine)

![Alibaba](assets/images/alibaba.png "Alibaba")

* 76% de conversions en plus entre navigateurs
* 14% en plus d'utilisateurs actifs/mois sous iOS; 30% en plus sous Android;
* 4x plus d'interaction à partir de "Add to Homescreen"

Autres : twitter lite, the guardian, pinterest

Note:
R: Comment vérifier qu'une application est conforme "PWA" ?
---

@title[Evaluation PWA]
## Comment évaluer une PWA ?

![Lighthouse](assets/images/pwa-lighthouse.png "Lighthouse")

Auditer son application : [Lighthouse](https://developers.google.com/web/tools/lighthouse/) 

Note:
J : je pars de zéro mais je voudrais une application mobile (Web + Stores). Comment je m'y prends ?
---

@title[Ionic]

![Ionic](assets/images/ionic.jpg "Ionic")

Note:
J: C'est tout ! Concrètement, c'est quoi son objectif ? 
+++

## Ionic - Objectif 

> Build amazing **apps** in **one codebase**, for **any platform**, with the **web**.

Note:
J: Y a quoi derrière Ionic ? 
+++

## Ionic - UI Kit

- [Composants imitation natif](https://ionicframework.com/docs/components/)
- [Wrapper fonctionnalités natives](https://ionicframework.com/docs/native/)
- CLI

Note:
J: Pour répondre à mon besoin initial, Ionic c'est un gros machin hybride ? 
+++

## Ionic - Cross Platform Hybrid
2 solutions possibles :
- Hydride : lancement dans [WebView](https://developer.android.com/reference/android/webkit/WebView.html)
- PWA : lancement dans navigateur

Note:
J: Ok mais c'est quoi l'intérêt d'une approche hybride avec Ionic comparée à une approche native ?
---

@title[Natif vs Hybride]
### Comparatif Natif / Hybride

<div class="comparatif">
    <table>
        <thead>
            <tr>
                <th>Attribute</th>
                <th>Native Approach</th>
                <th>Cross Platform Hybrid</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Developer Skill</td>
                <td><span class="ko"></span> Set Needed Objective-C, iOS SDK, Java, Android SDK</td>
                <td><span class="ok"></span> HTML, CSS, Javascript</td>
            </tr>
            <tr>
                <td>Distribution Method</td>
                <td>App Stores</td>
                <td><span class="ok"></span><br/>App Stores<br/>Desktop Browser<br/>Desktop App (e.g. Electron)<br/>Mobile Browser<br/>Progressive Web App</td>
            </tr>
            <tr>
                <td>Speed to Develop</td>
                <td><span class="ko"></span> Slow</td>
                <td><span class="ok"></span> Fast</td>
            </tr>
            <tr>
                <td>Development Cost</td>
                <td><span class="ko"></span> High</td>
                <td><span class="ok"></span> Low</td>
            </tr>
            <tr>
                <td>Maintenance Cost</td>
                <td><span class="ko"></span> High</td>
                <td>Moderate/low</td>
            </tr>
            <tr>
                <td>Graphical Performance</td>
                <td><span class="ok"></span> High</td>
                <td>Moderate</td>
            </tr>
            <tr>
                <td>App Performance</td>
                <td><span class="ok"></span> High</td>
                <td>Driven by use case</td>
            </tr>
            <tr>
                <td>Access to native functionality</td>
                <td><span class="ok"></span> Full native library</td>
                <td><span class="warning"></span> Full native library (requires third-party plugins)</td>
            </tr>
            <tr>
                <td>UX consistency across platforms and devices</td>
                <td><span class="ko"></span> Requires separate apps</td>
                <td><span class="ok"></span> Yes</td>
            </tr>
        </tbody>
    </table>
</div>

Note:
J: Ok pour la solution hybride mais derrière c'est quoi la stack technique ?
---

@title[Stack Technique]
## Stack Technique

<table>
  <tr>
    <th>PWA</th>
    <th>Hybride</th>
  </tr>
  <tr>
    <td style="text-align:center;" colspan="2">Angular</td>
  </tr>
  <tr>
    <td style="text-align:center;" colspan="2">Ionic</td>
  </tr>
  <tr>
    <td>API JS HTML5</td>
    <td>Cordova/PhoneGap</td>
  </tr>
</table>

Note:
J: Concrètement dans le code, ça rend quoi ? ça a la même tête qu'une application Angular ?  
---

@title[Un peu de code...]
## A quoi ressemble le code ?
- Projet Angular avec modules Ionic
- Typescript
- HTML / SASS

Note:
J: Et les caractéristiques PWA dans tout ça; ça se manifeste comment le côté installable ?
---

@title[Web App Manifest]
## Web App Manifest

Le `manifest.json` définit les propriétés de l'application :
- Nom
- Icônes
- Page d'accueil
- Orientation
- Navigateur visible ou non ?

Note:
J: Concernant la gestion hors ligne, que va t'on utiliser côté implémentation ?
---

@title[Service Worker]
## Service Worker - Définition
[Mozilla](https://developer.mozilla.org/fr/docs/Web/API/Service_Worker_API)
> Un service worker (...) n'a donc pas d'accès au DOM, et s'exécute dans une **tâche différente** de celle **du script principal** de votre application, ainsi il est **non-bloquant**.

+++

## Service Worker - Cycle de vie
![Service Worker](assets/images/sw.png "Service Worker")

+++

## Service Worker - Configuration
- `service-worker.js`
- Gestion par événements `self.addEventListener('event', ...)`
- Stratégie de cache avec [Google sw-toolbox](https://googlechromelabs.github.io/sw-toolbox/api.html)


Note:
J: Sur la partie réengagement de l'utilisateur, que dispose t'on ?
---

@title[Notifications]
## Notifications
- Notifications persistantes
- Push API
- SW + Push Manager

+++

## Workflow Push API

![Workflow Push API](assets/images/notif_push_api.png "Workflow Push API")

Note:
J: Concrètement, ça rend quoi ?

+++

## [Démo Notifications](https://technoz-pwa.herokuapp.com/subscription/broadcast?msg=Retrouvez%20le%20technoz%20PWA%20sous%20Gitlab)
![Démo time](assets/images/template-minion2.gif "Démo Time")
---

@title[Questions]

## Merci pour votre attention @fa[smile-o]
### Des questions ?

<div class="left">
    Julien MOURGUES<br/>
    @fa[twitter](@julm82)<br/>
    @fa[github] [Github](https://github.com/julienmourgues)<br/>
    @fa[gitlab] [Gitlab](https://gitlab.com/users/julienmourgues/projects)<br/>
    @fa[linkedin] [LinkedIn](https://www.linkedin.com/in/julien-mourgues/)

</div>
<div class="right">
    Rémi PICARD<br/>
    @fa[github] [Github](https://github.com/remi-picard)<br/>
    @fa[linkedin] [LinkedIn](https://www.linkedin.com/in/r%C3%A9mi-picard/)
</div>

---

@title[Ressources]
## Ressources
- [Présentation GitPitch Technoz PWA](https://gitpitch.com/julienmourgues/technozPwa/master?grs=gitlab) : https://gitpitch.com/julienmourgues/technozPwa/master?grs=gitlab
- [PWA du technoz](https://technoz-pwa.herokuapp.com) : https://technoz-pwa.herokuapp.com
- [Code source de la PWA](https://gitlab.com/julienmourgues/technozPwa) : https://gitlab.com/julienmourgues/technozPwa

---

@title[Références]
## Références
- [Google - PWA](https://developers.google.com/web/progressive-web-apps/)
- [Google - Lighthouse](https://developers.google.com/web/tools/lighthouse/)
- [Caractéristiques PWA](https://www.thurrott.com/windows/windows-10/116101/microsoft-said-progressive-web-apps-build)
- [Liste d'applications PWA](https://pwa.rocks/)
- [What web can do today ?](https://whatwebcando.today/)
- [Nantes JUG - Native apps are doomed](http://nantesjug.org/#/events/2018_01_31)
- [PWA vs Hybrid vs Native Apps](http://www.ignivadigital.com/insights/progressive-web-hybrid-native-apps/)
- [Schéma SW](http://webagility.com/posts/how-progressive-web-apps-make-the-web-great-again)
- [W3C - Push API](https://www.w3.org/TR/push-api/)